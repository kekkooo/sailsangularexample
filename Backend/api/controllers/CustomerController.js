/**
 * CustomerController
 *
 * @description :: Server-side logic for managing Customers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
module.exports = {
	index: function(req, res){
     
     customer.createNew({
         firstName: 'alessio',
         lastName:'murru',
         emailAddress:'alessio.murru@gmail.com'
     }, function(err, created){
         if(err){ 
             console.error(err);
             return res.badRequest('Something went wrong when saving a customer');
            }
         else{
             console.log('created customer :' + created);
             return res.ok();
         }
     });       
    }
};

