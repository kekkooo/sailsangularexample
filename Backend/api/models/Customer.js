/**
 * Customer.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
module.exports = {
  identity : 'customer',
  connection: 'localMysql',
  
  attributes:{
    firstName:'string',
    lastName: 'string',
    emailAddress: 'email'
  },
  
  createNew: function(input, cb){
    customer.create({
      firstName: input.firstName,
      lastName: input.lastName,
      emailAddress: input.emailAddress
    }).exec(cb);
  }
};
