/**
 * Waterpool.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
var Waterline = require('waterline');
var sailsMysqlAdapter = require('sails-mysql');

var Waterpool = Waterline.Collection.extend({
  identity : 'waterpool',
  connection: 'localMysql',
  
  attributes:{
    name:'string',
    address: 'string'
  }
}); 

module.exports = Waterpool;
/*
exports.createNew = function( name, surname, email ){
  Customer.create({
    firstName : name,
    lastName : surname,
    emailAddress : email
  })
};
*/