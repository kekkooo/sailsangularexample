import {Hero} from './hero';

export var HEROES: Hero[] = [
    {"id":11, "name": "Mr. Nice"},
    {"id":12, "name": "Narco"},
    {"id":13, "name": "Bombasto"},
    {"id":14, "name": "F4br1k"},
    {"id":15, "name": "C4rc4"},
    {"id":16, "name": "RubberMan"},
    {"id":17, "name": "Dynama"},
    {"id":18, "name": "Dr IQ"},
    {"id":19, "name": "MAgma"},
    {"id":20, "name": "Tornado"}
];